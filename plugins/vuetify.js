import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    // primary: '#121212', // a color that is not in the material colors palette
    primary: "#0D47A1", // a color that is not in the material colors palette
    accent: "#B0BEC5",
    // secondary: colors.amber.darken3,
    secondary: "#F9FBE7",
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    error: colors.deepOrange.accent4,
    success: colors.green.accent3
  }
})
